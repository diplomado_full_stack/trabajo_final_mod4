--Crear la base de datos
/*CREATE DATABASE diplomado_fullstack
    WITH 
    OWNER = postgres
    ENCODING = 'UTF8'
    LC_COLLATE = 'Spanish_Latin America.1252'
    LC_CTYPE = 'Spanish_Latin America.1252'
    TABLESPACE = pg_default
    CONNECTION LIMIT = -1;
*/

CREATE TABLE public.usuarios
(
    id integer NOT NULL DEFAULT nextval('usuarios_id_seq'::regclass),
    nombre character varying(255) NOT NULL,
    correo character varying(255)  NOT NULL,
    contrasenia character varying(255)  NOT NULL,
    estado boolean NOT NULL DEFAULT true,
    PRIMARY KEY (id)
);

CREATE TABLE public.categorias
(
    id integer NOT NULL DEFAULT nextval('categorias_id_seq'::regclass),
    nombre character varying(255) NOT NULL,
    usuarioid integer NOT NULL,
    PRIMARY KEY (id),
    CONSTRAINT categorias_usuarioid_fkey FOREIGN KEY (usuarioid)
        REFERENCES public.usuarios (id) MATCH SIMPLE
        ON UPDATE CASCADE
        ON DELETE CASCADE
);

CREATE TABLE public.productos
(
    id integer NOT NULL DEFAULT nextval('productos_id_seq'::regclass),
    nombre character varying(255) NOT NULL,
    precio_unitario numeric(10,2) DEFAULT 0,
    estado boolean NOT NULL DEFAULT true,
    categoriaid integer,
    usuarioid integer,
    PRIMARY KEY (id),
    CONSTRAINT productos_categoriaid_fkey FOREIGN KEY (categoriaid)
        REFERENCES public.categorias (id) MATCH SIMPLE
        ON UPDATE CASCADE
        ON DELETE SET NULL,
    CONSTRAINT productos_productoid_fkey FOREIGN KEY (usuarioid)
        REFERENCES public.usuarios (id) MATCH SIMPLE
        ON UPDATE CASCADE
        ON DELETE SET NULL
);

INSERT INTO public.usuarios (id, nombre, correo, contrasenia, estado) VALUES (1, 'Alan', 'alanvba@gmail.com', '123', true);
INSERT INTO public.usuarios (id, nombre, correo, contrasenia, estado) VALUES (2, 'Carla', 'carla@gmail.com', '123', true);

INSERT INTO public.categorias (id, nombre, usuarioid) VALUES (1, 'Electrodomestico', 1);
INSERT INTO public.categorias (id, nombre, usuarioid) VALUES (2, 'Laptop', 1);
INSERT INTO public.categorias (id, nombre, usuarioid) VALUES (4, 'IA', 2);

INSERT INTO public.productos (id, nombre, precio_unitario, estado, categoriaid, usuarioid) VALUES (1, 'Teclado', 80.50, true, 1, 1);
INSERT INTO public.productos (id, nombre, precio_unitario, estado, categoriaid, usuarioid) VALUES (2, 'Mouse', 500.50, true, 1, 1);