import { Productos } from '../models/Productos.js';
import { Categoria } from '../models/Categorias.js';
import { Usuarios } from '../models/Usuarios.js';
import logger from '../logs/logger.js';

export async function getAll(req, res) {
  try {
    const producto = await Productos.findAll({
      attributes: ['id', 'nombre', 'precio_unitario', 'estado', 'categoriaid','usuarioid'],
    });
    logger.info(`Consulta realizada correctamente producto.controller metodo getAll()`);
    res.json(producto);
  } catch (error) {
    logger.error(`error en la consulta usuario categoria.controller metodo getAll() ${error.message}`);
    res.status(500).json({
      message: error.message,
    });
  }
}

export async function insert(req, res) {
  console.log('creando producto', req.body);
  const { nombre, precio_unitario, estado, categoriaid,usuarioid } = req.body;
  try {
    const nuevoProducto = await Productos.create(
      {
        nombre, 
        precio_unitario, 
        estado, 
        categoriaid,
        usuarioid,
      },
      {
        fields: ['nombre', 'precio_unitario', 'estado', 'categoriaid','usuarioid'],
      }
    );
    logger.info(`Consulta realizada correctamente producto.controller metodo insert()`);
    return res.json(nuevoProducto);
  } catch (error) {
    logger.error(`error en la consulta usuario categoria.controller metodo insert() ${error.message}`);
    res.status(500).json({
      message: error.message,
    });
  } 
}

export async function getById(req, res) {
  const { id } = req.params;
  try {
    const producto = await Productos.findOne({
      where: { id },
    });
    logger.info(`Consulta realizada correctamente producto.controller metodo getById()`);
    return res.json(producto);
  } catch (error) {
    logger.error(`error en la consulta usuario categoria.controller metodo getById() ${error.message}`);
    res.status(500).json({
      message: error.message,
    });
  }
}

export async function updateProducto(req, res) {
  const { id } = req.params;
  const { nombre, precio_unitario, estado, categoriaid,usuarioid } = req.body;

  try {
    const producto = await Productos.findByPk(id);
    producto.nombre = nombre;
    producto.precio_unitario = precio_unitario;
    producto.estado = estado;
    producto.categoriaid = categoriaid;
    producto.usuarioid = usuarioid;

    await producto.save();
    logger.info(`Consulta realizada correctamente producto.controller metodo updateProducto()`);
    return res.json(producto);
  } catch (error) {
    logger.error(`error en la consulta usuario categoria.controller metodo updateProducto() ${error.message}`);
    res.status(500).json({
      message: error.message,
    });
  }
}

export async function eliminar(req, res) {
  const { id } = req.params;
  try {
    await Productos.destroy({
      where: { id },
    });
    logger.info(`Consulta realizada correctamente producto.controller metodo eliminar()`);
    return res.sendStatus(204);
  } catch (error) {
    logger.error(`error en la consulta usuario categoria.controller metodo eliminar() ${error.message}`);
    res.status(500).json({
      message: error.message,
    });
  }
}

export async function getProductoForUsuario(req, res) {
  const { id } = req.params;
  try {
    const producto = await Productos.findAll({
      attributes: ['nombre', 'precio_unitario', 'estado', 'categoriaid','usuarioid'],
      where: { usuarioid: id },
    });
    logger.info(`Consulta realizada correctamente producto.controller metodo getProductoForUsuario()`);
    return res.json(producto);
  } catch (error) {
    logger.error(`error en la consulta usuario categoria.controller metodo getProductoForUsuario() ${error.message}`);
    res.status(500).json({
      message: error.message,
    });
  }
}

export async function getProductoWithCategoria(req, res) {
  try {
    const producto = await Productos.findAll({
      attributes: ['id','nombre', 'precio_unitario', 'estado', 'categoriaid','usuarioid'],
      include: [
        {
          model: Categoria,
          attributes: ['id', 'nombre'],
          required: true,
        },
      ],
    });
    logger.info(`Consulta realizada correctamente producto.controller metodo getProductoWithCategoria()`);
    res.json(producto);
  } catch (error) {
    logger.error(`error en la consulta usuario categoria.controller metodo getProductoWithCategoria() ${error.message}`);
    res.status(500).json({
      message: error.message,
    });
  }
}

export async function getEstado(req, res) {
    try {
      const estado = {"Message":"El servicio de producto esta activo"};
      logger.info(`Consulta realizada correctamente producto.controller metodo getEstado()`);
      return res.json(estado);
    } catch (error) {
      logger.warn(`error en la consulta estado ws usuario ${error.message}`);
      res.status(500).json({
        message: error.message,
      });
    }
  }