import { Usuarios } from '../models/Usuarios.js';
import { Categoria } from '../models/Categorias.js';
import logger from '../logs/logger.js';
import jwt from 'jsonwebtoken';
import { token } from 'morgan';
import 'dotenv/config'; 

const KEY_TOKEN = process.env.KEY_PRIVACITY;

export async function login(req, res){
  try {
    const { nombre, correo, contrasenia} = req.body;

    const usuariologeado = await Usuarios.findOne({
      where: { correo: correo,contrasenia:contrasenia },
    });

    jwt.sign({usuariologeado},KEY_TOKEN,(err,token)=>{
      res.json({token});
    });

    logger.info(`se logeo correctamente usuario.controller metodo login(): ${nombre} ${correo}`);
  } catch (error) {
    logger.warn(`error en la consulta usuario usuario.controller metodo login() ${error.message}`);
    res.status(500).json({
      message: error.message,
    });
  }
}

export function verifyToken(req, res, next){
  const beareHeader = req.headers['authorization'];
  logger.info(`Iniciando verificacion token`);
  if(typeof beareHeader !== 'undefined'){
    const token = beareHeader.split(' ')[1];
    logger.info(`obtenido token ${token}`);
    jwt.verify(token,KEY_TOKEN,(error,usuario)=>{
      if(error) res.sendStatus(403);
      else {
        next();
      }
    });
  }else res.sendStatus(403);
}

export async function getList(req, res) {
  try {
    const usuario = await Usuarios.findAll({
      attributes: ['id', 'nombre', 'correo', 'contrasenia', 'estado'],
    });
    logger.info(`Consulta realizada correctamente usuario.controller metodo getList()`);
    res.json(usuario);
  } catch (error) {
    logger.warn(`error en la consulta usuario usuario.controller metodo getList() ${error.message}`);
    res.status(500).json({
      message: error.message,
    });
  }
}

export async function add(req, res) {
  console.log('creando usuario', req.body);
  const { nombre, correo, contrasenia, estado } = req.body;
  try {
    const nuevoUsuario = await Usuarios.create(
      {
        nombre, 
        correo, 
        contrasenia, 
        estado,
      },
      {
        fields: ['nombre', 'correo', 'contrasenia', 'estado'],
      }
    );
    logger.info(`Consulta realizada correctamente usuario.controller metodo add()`);
    return res.json(nuevoUsuario);
  } catch (error) {
    logger.warn(`error en la consulta usuario usuario.controller metodo add() ${error.message}`);
    res.status(500).json({
      message: error.message,
    });
  } 
}

export async function getById(req, res) {
  const { id } = req.params;
  try {
    const usuario = await Usuarios.findOne({
      where: { id },
    });
    logger.info(`Consulta realizada correctamente usuario.controller metodo getById()`);
    return res.json(usuario);
  } catch (error) {
    logger.warn(`error en la consulta usuario usuario.controller metodo getById() ${error.message}`);
    res.status(500).json({
      message: error.message,
    });
  }
}

export async function updateUsuario(req, res) {
  const { id } = req.params;
  const { nombre, correo, contrasenia, estado } = req.body;

  try {
    const usuario = await Usuarios.findByPk(id);
    usuario.nombre = nombre;
    usuario.correo = correo;
    usuario.contrasenia = contrasenia;
    usuario.estado = estado;

    await usuario.save();
    logger.info(`Consulta realizada correctamente usuario.controller metodo updateUsuario()`);
    return res.json(usuario);
  } catch (error) {
    logger.warn(`error en la consulta usuario usuario.controller metodo updateUsuario() ${error.message}`);
    res.status(500).json({
      message: error.message,
    });
  }
}

export async function deleteUsuario(req, res) {
  const { id } = req.params;
  try {

    const categorias = await Categoria.findAll({
      attributes: ['id', 'nombre', 'usuarioid'],
      where: { usuarioid: id },
    });

    if(categorias.length>0){
      return res.json({message:"No se puede eliminar el usuario, existe categorias relacionadas."});
    }
    await Usuarios.destroy({
      where: { id },
    });
    logger.info(`Consulta realizada correctamente usuario.controller metodo deleteUsuario()`);
    return res.sendStatus(204);
  } catch (error) {
    logger.warn(`error en la consulta usuario usuario.controller metodo deleteUsuario() ${error.message}`);
    res.status(500).json({
      message: error.message,
    });
  }
}

export async function getUsuarioCategorias(req, res) {
  const { id } = req.params;
  try {
    const categorias = await Categoria.findAll({
      attributes: ['id', 'nombre', 'usuarioid'],
      where: { usuarioid: id },
    });
    logger.info(`Consulta realizada correctamente usuario.controller metodo getUsuarioCategorias()`);
    return res.json(categorias);
  } catch (error) {
    logger.warn(`error en la consulta usuario usuario.controller metodo getUsuarioCategorias() ${error.message}`);
    res.status(500).json({
      message: error.message,
    });
  }
}

export async function getUsuariosCategoria(req, res) {
  try {
    const usuarios = await Usuarios.findAll({
      attributes: ['id', 'nombre', 'correo', 'contrasenia', 'estado'],
      include: [
        {
          model: Categoria,
          attributes: ['id', 'nombre'],
          required: true,
        },
      ],
    });
    logger.info(`Consulta realizada correctamente usuario.controller metodo getUsuariosCategoria()`);
    return res.json(usuarios);
  } catch (error) {
    logger.warn(`error en la consulta usuario usuario.controller metodo getUsuariosCategoria() ${error.message}`);
    res.status(500).json({
      message: error.message,
    });
  }
}

export async function getEstado(req, res) {
  try {
    logger.info(`Consulta realizada correctamente usuario.controller metodo getEstado()`);
    const estado = {"Message":"El servicio de usuarios esta activo"};
    return res.json(estado);
  } catch (error) {
    logger.warn(`error en la consulta usuario usuario.controller metodo getEstado() ${error.message}`);
    res.status(500).json({
      message: error.message,
    });
  }
}