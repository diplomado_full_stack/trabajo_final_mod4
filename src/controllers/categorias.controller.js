import { Categoria } from '../models/Categorias.js';
import { Productos } from '../models/Productos.js';
import logger from '../logs/logger.js';

export async function getAll(req, res) {
  try {
    const categoria = await Categoria.findAll({
      attributes: ['id', 'nombre', 'usuarioid'],
    });
    logger.info(`Consulta realizada correctamente categoria.controller metodo findAll()`);
    res.json(categoria);
  } catch (error) {
    logger.error(`error en la consulta usuario categoria.controller metodo getAll() ${error.message}`);
    res.status(500).json({
      message: error.message,
    });
  }
}

export async function insert(req, res) {
  console.log('save categoria.', req.body);
  const { nombre, usuarioid } = req.body;
  try {
    const newCategoria = await Categoria.create(
      {
        nombre, 
        usuarioid, 
      },
      {
        fields: ['nombre', 'usuarioid'],
      }
    );
    logger.info(`Consulta realizada correctamente categoria.controller metodo insert()`);
    return res.json(newCategoria);
  } catch (error) {
    logger.error(`error en la consulta usuario categoria.controller metodo insert() ${error.message}`);
    res.status(500).json({
      message: error.message,
    });
  }
}

export async function getById(req, res) {
  const { id } = req.params;
  try {
    const categoria = await Categoria.findOne({
      where: { id },
    });
    logger.info(`Consulta realizada correctamente categoria.controller metodo getById()`);
    return res.json(categoria);
  } catch (error) {
    logger.error(`error en la consulta usuario categoria.controller metodo getById() ${error.message}`);
    res.status(500).json({
      message: error.message,
    });
  }
}

export async function update(req, res) {
  const { id } = req.params;
  const { nombre, usuarioid } = req.body;

  try {
    const categoria = await Categoria.findByPk(id);
    categoria.nombre = nombre;
    categoria.usuarioid = usuarioid;

    await categoria.save();
    logger.info(`Consulta realizada correctamente categoria.controller metodo update()`);
    return res.json(categoria);
  } catch (error) {
    logger.error(`error en la consulta usuario categoria.controller metodo update() ${error.message}`);
    res.status(500).json({
      message: error.message,
    });
  }
}

export async function eliminar(req, res) {
  const { id } = req.params;
  try {
    await Categoria.destroy({
      where: { id },
    });
    logger.info(`Consulta realizada correctamente categoria.controller metodo eliminar()`);
    return res.sendStatus(204);
  } catch (error) {
    logger.error(`error en la consulta usuario categoria.controller metodo eliminar() ${error.message}`);
    res.status(500).json({
      message: error.message,
    });
  }
}

export async function getCategoriasForUsuario(req, res) {
  const { id } = req.params;
  try {
    const categorias = await Categoria.findAll({
      attributes: ['id', 'nombre', 'usuarioid'],
      where: { usuarioid: id },
    });
    logger.info(`Consulta realizada correctamente categoria.controller metodo getCategoriasForUsuario()`);
    return res.json(categorias);
  } catch (error) {
    logger.error(`error en la consulta usuario categoria.controller metodo getCategoriasForUsuario() ${error.message}`);
    res.status(500).json({
      message: error.message,
    });
  }
}

export async function getCategoriaWithProductos(req, res) {
  try {
    const categoria = await Categoria.findAll({
      attributes: ['id', 'nombre','usuarioid'],
      include: [
        {
          model: Productos,
          attributes: ['id', 'nombre','precio_unitario','estado'],
          required: true,
        },
      ],
    });
    logger.info(`Consulta realizada correctamente categoria.controller metodo getCategoriaWithProductos()`);
    res.json(categoria);
  } catch (error) {
    logger.error(`error en la consulta usuario categoria.controller metodo getCategoriaWithProductos() ${error.message}`);
    res.status(500).json({
      message: error.message,
    });
  }
}

export async function getEstado(req, res) {
  try {
    const estado = {"Message":"El servicio de categoria esta activo"};
    return res.json(estado);
  } catch (error) {
    logger.error(`error en la consulta usuario categoria.controller metodo getEstado() ${error.message}`);
    res.status(500).json({
      message: error.message,
    });
  }
}