import { Router } from 'express';
import {
  getEstado,
  getAll,
  insert,
  getById,
  update,
  eliminar,
  getCategoriasForUsuario,
  getCategoriaWithProductos,
} from '../controllers/categorias.controller.js';
import {
  verifyToken
} from '../controllers/usuario.controller.js';
const router = Router();

// Routes
router.get('/estado',verifyToken, getEstado);

router.get('/',verifyToken, getAll);

router.post('/',verifyToken, insert);

router.get('/:id',verifyToken, getById);

router.put('/:id',verifyToken, update);

router.delete('/:id',verifyToken, eliminar);

router.get('/:id/categorias',verifyToken, getCategoriasForUsuario);

router.get('/all/categorias/all',verifyToken, getCategoriaWithProductos);

export default router;