import { Router } from 'express';
import {
  getEstado,
  getAll,
  insert,
  getById,
  updateProducto,
  eliminar,
  getProductoForUsuario,
  getProductoWithCategoria,
} from '../controllers/productos.controller.js';
import {
  verifyToken
} from '../controllers/usuario.controller.js';

const router = Router();

// Routes
router.get('/estado',verifyToken, getEstado);

router.get('/',verifyToken, getAll);

router.post('/',verifyToken, insert);

router.get('/:id',verifyToken, getById);

router.put('/:id',verifyToken, updateProducto);

router.delete('/:id',verifyToken, eliminar);

router.get('/:id/producto',verifyToken, getProductoForUsuario);

router.get('/all/producto/all',verifyToken, getProductoWithCategoria);

export default router;