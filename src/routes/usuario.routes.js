import { Router } from 'express';
import {
  getEstado,
  getList,
  add,
  getById,
  updateUsuario,
  deleteUsuario,
  getUsuarioCategorias,
  getUsuariosCategoria,
  login,
  verifyToken
} from '../controllers/usuario.controller.js';

const router = Router();

// Routes
router.get('/estado',verifyToken, getEstado);

router.get('/',verifyToken, getList);

router.post('/',verifyToken, add);

router.get('/:id',verifyToken, getById);

router.put('/:id',verifyToken, updateUsuario);

router.delete('/:id',verifyToken, deleteUsuario);

router.get('/:id/categorias',verifyToken, getUsuarioCategorias);

router.get('/all/usuarios/all',verifyToken, getUsuariosCategoria);

router.post('/login', login);

export default router;