import { Sequelize } from 'sequelize';
import 'dotenv/config'; 

const BASEDATOS = process.env.DB_NOMBRE_BASE_DATOS;
const USER = process.env.DB_USER;
const DB_PASSWORD = process.env.DB_PASSWORD;
const HOST = process.env.HOST;

export const sequelize = new Sequelize(
  BASEDATOS,
  USER,
  DB_PASSWORD,
  {
    host: HOST,
    dialect: 'postgres',
  }
);