import express from 'express';
import morgan from 'morgan';

const app=express();
//importacion de rutas
import usuarioRoutes from './routes/usuario.routes.js';
import categoriasRoutes from './routes/categorias.routes.js';
import productoRoutes from './routes/productos.routes.js';

// Middlewares
app.use(morgan('dev'));
app.use(express.json());

// Routes
app.use('/api/v1.0/usuario', usuarioRoutes);
app.use('/api/v1.0/categorias', categoriasRoutes);
app.use('/api/v1.0/productos', productoRoutes);

export default app;