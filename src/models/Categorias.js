import { DataTypes } from 'sequelize';
import { sequelize } from '../database/database.js';
import { Productos } from './Productos.js';

export const Categoria = sequelize.define(
  'categorias',
  {
    id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true,
    },
    nombre: {
      type: DataTypes.STRING,
      allowNull: false,
      comment: 'nombre de la categoria',
    },
  },
  {
    timestamps: false,
  }
);


Categoria.hasMany(Productos, {
    foreignKey: 'categoriaid',
    sourceKey: 'id',
  });
  
  Productos.belongsTo(Categoria, {
    foreignKey: 'categoriaid',
    targetKey: 'id',
  });