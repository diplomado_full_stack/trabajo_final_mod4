import { DataTypes } from 'sequelize';
import { sequelize } from '../database/database.js';
import { Categoria } from './Categorias.js';
import { Productos } from './Productos.js'

export const Usuarios = sequelize.define(
  'usuarios',
  {
    id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true,
    },
    nombre: {
      type: DataTypes.STRING,
      allowNull: false,
      comment: 'nombre del usuario',
    },
    correo: {
      type: DataTypes.STRING,
      allowNull: false,
      comment: 'correo del usuario',
    },
    contrasenia: {
        type: DataTypes.STRING,
        allowNull: false,
        comment: 'contrasenia del usuario',
    },
    estado: {
        type: DataTypes.BOOLEAN,
        allowNull: false,
        defaultValue: true,
        comment: 'estado del usuario',
    },
  },
  {
    timestamps: false,
  }
);

Usuarios.hasMany(Categoria, {
  foreignKey: {
    name: 'usuarioid',
    allowNull: false
  },
    sourceKey: 'id',
  });

Categoria.belongsTo(Usuarios, {
    foreignKey: 'usuarioid',
    targetKey: 'id',
  });

Usuarios.hasMany(Productos, {
    foreignKey: 'usuarioid',
    sourceKey: 'id',
  });

Productos.belongsTo(Usuarios, {
    foreignKey: 'usuarioid',
    targetKey: 'id',
  });