import { DataTypes } from 'sequelize';
import { sequelize } from '../database/database.js';

export const Productos = sequelize.define(
  'productos',
  {
    id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true,
    },
    nombre: {
      type: DataTypes.STRING,
      allowNull: false,
      comment: 'nombre del producto',
    },
    precio_unitario: {
      type: DataTypes.DECIMAL(10, 2),
      defaultValue: 0.00,
      comment: 'Precio unitario',
    },
    estado: {
        type: DataTypes.BOOLEAN,
        allowNull: false,
        defaultValue: true,
        comment: 'estado del producto',
    },
  },
  {
    timestamps: false,
  }
);