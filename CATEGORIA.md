API REST - Documentación Sistema examen final modulo 4
### Dominio:
localhost:3000/api/v1.0/usuario

### Modulo 2: Categoria
Estado - Estado de la API REST
(GET)
/estado

Listar todos las categorias
(GET)
/

Inserta una nueva categoria
(POST)
/

Buscar una categoria por ID
(GET)
/:id

Actualiza una categoria buscando por ID
(PUT)
/:id

Elimina una categoria
(DELETE)
/:id

Lista todos las categorias por usuario
(GET)
/:id/categorias

lista las categorias incluyendo sus productos
(GET)
/all/categorias/all
