API REST - Documentación Sistema examen final modulo 4
### Dominio:
localhost:3000/api/v1.0/usuario

### Modulo 3: Producto
Estado - Estado de la API REST
(GET)
/estado

Listar todos los productos
(GET)
/

Inserta un nuevo producto
(POST)
/

Buscar un producto por ID
(GET)
/:id

Actualiza un producto buscando por ID
(PUT)
/:id

Elimina una producto
(DELETE)
/:id

Lista todos los Producto por categoria
(GET)
/:id/producto

lista los productos incluyendo sus categorias
(GET)
/all/producto/all
