API REST - Documentación Sistema examen final modulo 4
### Dominio:
localhost:3000/api/v1.0/usuario

### Modulo 1: Usuario
Estado - Estado de la API REST
(GET)
/estado

Listar todos los usuarios
(GET)
/

Inserta un nuevo usuario
(POST)
/

Buscar un usuarios por ID
(GET)
/:id

Actualiza un registro de usuario buscando por ID
(PUT)
/:id

Elimina un usuario siempre y cuando no tenga ninguna categoria
(DELETE)
/:id

Lista todos los usuario de una categoria
(GET)
/:id/categorias

lista los usuarios incluido las categorias
(GET)
/all/usuarios/all
